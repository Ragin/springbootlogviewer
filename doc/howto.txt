FLYWAY
    CONFIG
        Configuration within pom.xml
            jdbc:h2:file:./database/persons
            user: sa
            pw:
            migration directory: ./src/main/resources/db/migration/
            database location from config:  SpringbootLogviewer\database\customer.mv.db

    CMDS
        mvn flyway:info
        mvn flyway:migrate
