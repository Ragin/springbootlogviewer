package org.ragin.logviewer.controller;

import org.ragin.logviewer.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class LogviewerController {
    @RequestMapping("/log")
    public String main(Model model) {
        return "log"; // thymeleaf
    }
}
