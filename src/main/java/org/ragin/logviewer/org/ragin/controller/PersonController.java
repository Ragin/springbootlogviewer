package org.ragin.logviewer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.ragin.logviewer.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.*;
import java.util.*;

import org.ragin.logviewer.model.Person;

@Controller
public class PersonController {
    /**
     * maps the / route
     *
     * @return
     */
    @RequestMapping("/")
    public String main(Model model) {
        return "landingPage"; // thymeleaf
    }

    // /hello?name=kotlin
    @GetMapping("/randomPerson")
    public String mainWithParam(@RequestParam(name = "name", required = false, defaultValue = "") String name, Model model) {
        List<Person> persons = Person.getDemoPersons();
        List<String> personData = new ArrayList<String>();
        int personPointer = randomInt(0, persons.size() - 1); // starting from 0

        personData.add("Prename:        " + persons.get(personPointer).getPreName());
        personData.add("Middle name:    " + persons.get(personPointer).getMiddleName());
        personData.add("Last name:      " + persons.get(personPointer).getLastName());
        personData.add("Age:            " + persons.get(personPointer).getAge());
        personData.add("City:           " + persons.get(personPointer).getCity());
        personData.add("Comment:        " + persons.get(personPointer).getComment());
        model.addAttribute("prename", persons.get(personPointer).getFullName());
        model.addAttribute("personData", personData);
        return "person"; //view
    }

    /**
     * create a raondom integer
     *
     * @param min minimum
     * @param max maximum
     * @return random integer
     */
    public int randomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    private static void demoJava8Streams2() {
        List<Person> personen = Person.getDemoPersons();

        System.out.println("\nGirls:");
        Stream<Person> girls = personen.stream().filter(Person::isFemale);
        Stream<Person> men = personen.stream().filter(Person::isMale);
        girls.forEach(System.out::println);

        // filtering
        System.out.println("\nMen:");
        men.forEach(System.out::println);

        System.out.println("\nYoung girls:");
        Stream youngGirls = personen
                .stream()
                .filter(Person::isFemale)
                .filter(currentPerson -> currentPerson.getAge() <= 25);// lambda
        youngGirls.forEach(System.out::println);

        System.out.println("\nOnly one twin:");
        Stream onlyStefanie = personen
                .stream()
                .filter(Person::isFemale)
                .filter(currentPerson -> currentPerson.getAge() <= 25) // lambda
                .filter(cp -> cp.getPreName().indexOf("fan") > 0);// lambda
        onlyStefanie.forEach(System.out::println);

        // mapping
        Stream<String> girlsNamesStream = personen
                .stream()
                .filter(Person::isFemale)
                .map(bla -> bla.getPreName()); // maps object stream to string stream

        System.out.println("\nGirls names:");
        girlsNamesStream.forEach(System.out::println);
    }

    private static void demoJava8Streams1() {
        List<Person> personen = Person.getDemoPersons();
        personen.forEach((aSinglePerson) -> {
            aSinglePerson.identiyfy();
        }); // Hello, my name is Peter  Pan and I live in Rostock. My age is 28 years.
        for (Person person : personen) {
            System.out.println(person.identiyfy());
        }
        personen.forEach(System.out::println); // org.ragin.model.Person@58d25a40
    }

    private static void demoJava8Println() {
        System.out.println("Hello World!");
        String[] girls = new String[]{"Kerstin", "Birgit", "Verena"};
        String[] myStringArray1 = {"a", "b", "c"};
        String[] myStringArray2 = new String[]{"a", "b", "c"};
        List<String> strings = Arrays.asList(new String[]{"one", "two", "three"});
        System.out.println("Hello Codiva");
        Arrays.asList(girls).forEach(System.out::println);
        strings.forEach(System.out::println);
    }
}
