package org.ragin.logviewer.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;

public class Person implements Serializable {
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public enum Sex {
        MALE, FEMALE
    }

    @Id
    @GeneratedValue
    private long id;

    private String address;
    private String city;
    private String cob; // city of birth
    private String comment;
    private String country;
    private String dob; // day of birth
    private String email;
    private String imageName;
    private String lastName;
    private String middleName;
    private String mobile;
    private String phone;
    private String preName;
    private Sex gender;
    private String title;
    private String website;
    private String zip;
    private LocalDate birthday;

    @ManyToOne
    private String Company;

    // private no-arg constructor is needed by JPA
    private Person() {

    }

    /**
     * constructor of person
     *
     * @param preName
     * @param lastName
     * @param city
     * @param gender   from enum
     * @param email    mail address
     * @param dob      day of birth
     * @param comment  any comment
     */
    public Person(String preName, String lastName, String city, Sex gender, String email, LocalDate dob, String comment) {
        this.city = city;
        this.comment = comment;
        this.email = email;
        this.lastName = lastName;
        this.preName = preName;
        this.gender = gender;
        this.setBirthday(dob);
    }

    /**
     * init block that sets demo values to a new person.
     * middlename is not set
     */ {
        setPreName("demo_preName");
        setMiddleName("");
        setLastName("demo_lastName");
        setAddress("demo_address");
        setZip("demo_zip");
        setCity("demo_city");
        setCountry("demo_country");
        setEmail("demo_email");
        setWebsite("demo_website");
        setPhone("demo_phone");
        setMobile("demo_mobile");
        setDob("demo_dob");
        setTitle("demo_title");
        setComment("demo_comment");
        setImageName("demo_imageName");
        setBirthday(IsoChronology.INSTANCE.date(1988, 8, 8));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Sex getGender() {
        return gender;
    }

    public void setGender(Sex gender) {
        this.gender = gender;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return preName + " " + lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getCob() {
        return cob;
    }

    public void setCob(String cob) {
        this.cob = cob;
    }

    /**
     * returns the age in years of this person
     *
     * @return
     */
    public int getAge() {
        return birthday.until(IsoChronology.INSTANCE.dateNow()).getYears();
    }

    /**
     * returns a String saying hello
     *
     * @return
     */
    public String identiyfy() {
        StringBuilder sb = new StringBuilder();
        sb.append("Hello, my name is ");
        sb.append(this.getPreName() + " ");
        sb.append(this.getMiddleName().length() > 0 ? " " + this.getMiddleName() + " " : " ");
        sb.append(this.getLastName());
        sb.append(" and I live in " + this.getCity() + ". ");
        sb.append("My age is " + getAge() + " years.");
        return sb.toString();
    }

    /**
     * returns a single demo Person
     *
     * @return
     */
    public static Person demoSinglePerson() {
        Person p = new Person("Birgit", "Meier", "Hattingen", Sex.FEMALE, "birgit@demo.net", IsoChronology.INSTANCE.date(1980, 6, 11), "Person 1");
        return p;
    }

    /**
     * creates a single demo Person and returns the String value of its identify method
     *
     * @return
     */
    public static String identifyDemoPerson() {
        Person p = new Person("Birgit", "Meier", "Hattingen", Sex.FEMALE, "birgit@demo.net", IsoChronology.INSTANCE.date(1980, 6, 11), "Person 1");
        return p.identiyfy();
    }

    /**
     * returns a list of 4 example Persons
     *
     * @return
     */
    public static List getDemoPersons() {
        List<Person> demo = new ArrayList<>();
        demo.add(new Person("Birgit", "Meier", "Hattingen", Sex.FEMALE, "birgit@demo.net", IsoChronology.INSTANCE.date(1980, 6, 11), "Person 1"));
        demo.add(new Person("Hans", "Dampf", "Bonn", Sex.MALE, "hans@demo.net", IsoChronology.INSTANCE.date(1966, 7, 12), "Person 2"));
        demo.add(new Person("Peter", "Pan", "Rostock", Sex.MALE, "peter@demo.net", IsoChronology.INSTANCE.date(1950, 5, 14), "Person 3"));
        demo.add(new Person("Sandra", "Sauer", "Wuppertal", Sex.FEMALE, "sandra@demo.net", IsoChronology.INSTANCE.date(1940, 6, 23), "Person 4"));
        demo.add(new Person("Kornelia", "Pticar", "Wuppertal", Sex.FEMALE, "cp@demo.net", IsoChronology.INSTANCE.date(1990, 6, 13), "Person 5"));
        demo.add(new Person("Kuno", "Karpfen", "Karlsruhe", Sex.MALE, "kk@demo.net", IsoChronology.INSTANCE.date(1996, 7, 12), "Person 6"));
        demo.add(new Person("Veronika", "Wolf", "Wien", Sex.FEMALE, "ww@demo.net", IsoChronology.INSTANCE.date(2001, 6, 3), "Person 7 - a twin"));
        demo.add(new Person("Stefanie", "Wolf", "Wien", Sex.FEMALE, "sw@demo.net", IsoChronology.INSTANCE.date(2001, 6, 3), "Person 8 - second twin"));
        return demo;
    }

    public boolean isFemale() {
        return (this.gender == Sex.FEMALE);
    }

    public boolean isMale() {
        return (this.gender == Sex.MALE);
    }

    public String toString() {
        return (this.preName + " " + this.middleName + " " + this.lastName + " from " + this.city + ", " + this.gender).replaceAll("\\s+", " ");
    }
}

