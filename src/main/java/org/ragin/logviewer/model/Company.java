package org.ragin.logviewer.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;
import java.util.*;
import org.ragin.logviewer.model.Person;

@Entity

public class Company {
    // for JPA
    private Company() {
    }

    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String city;
    @OneToMany(mappedBy = "company")
    private List<Person> employees;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Person> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Person> employees) {
        this.employees = employees;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
