CREATE TABLE Personen (
	id INT NOT NULL AUTO_INCREMENT,
	lastName VARCHAR,
	preName VARCHAR,
	middleName VARCHAR NOT NULL,
	address TEXT,
	city TEXT,
	cityOfBirth TEXT,
	country TEXT,
	dayOfBirth DATE,
	email TEXT,
	zip TEXT,
	website VARCHAR,
	phone VARCHAR,
	mobile VARCHAR,
	sex VARCHAR
);